# nexus3

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with nexus3](#setup)
    * [What nexus3 affects](#what-nexus3-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nexus3](#beginning-with-nexus3)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

The Puppet nexus3 module installs, configures and manages [Sonatype Nexus Repository 3](https://www.sonatype.com/nexus-repository-oss). The module expects that your system already has access to a Nexus RPM package in an installed Yum repository.

## Setup

To install Sonatype Nexus Repository 3 from an OS package in an installed repository:

```puppet
include 'nexus3'
```

## Usage

Defaults:

```puppet
include 'nexus3'
```

Remove package:

```puppet
class {'nexus3':
  ensure         => 'absent',
  manage_service => 'false',
}
```

## Reference

For information on classes, types and functions see the [REFERENCE.md](https://gitlab.com/harbottle/puppet-nexus3/blob/master/REFERENCE.md).

## Limitations

- Currently only works on EL7/8
- Currently expects an available OS package in a configured repository

## Development

Please send pull requests. 


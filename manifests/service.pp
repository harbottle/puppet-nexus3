# @summary Manage the Sonatype Nexus Repository 3 service
#
# @api private
#
class nexus3::service inherits nexus3 {
  assert_private()
  if $nexus3::service_manage {
    service { $nexus3::service_name:
      ensure => $nexus3::service_ensure,
      enable => $nexus3::service_enable,
    }
  }
}

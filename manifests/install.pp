# @summary Manage the Sonatype Nexus Repository 3 package
#
# @api private
#
class nexus3::install inherits nexus3 {
  assert_private()
  if $nexus3::package_manage {
    package { $nexus3::package_name:
      ensure => $nexus3::package_ensure,
    }
  }
}

# @summary Install and configure Sonatype Nexus Repository 3
#
# @api public
#
# @example Basic usage
#   include nexus3
class nexus3 (
  Boolean $package_manage,
  String  $package_name,
  String  $package_ensure,
  Boolean $service_manage,
  String  $service_name,
  String  $service_ensure,
  Boolean $service_enable,
) {
  contain nexus3::install
  contain nexus3::config
  contain nexus3::service
  Class['nexus3::install'] -> Class['nexus3::config'] ~> Class['nexus3::service']
  Class['nexus3::install'] ~> Class['nexus3::service']
}

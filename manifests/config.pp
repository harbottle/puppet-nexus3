# @summary Manage the Sonatype Nexus Repository 3 config
#
# @api private
#
class nexus3::config inherits nexus3 {
  assert_private()
}

# Changelog

All notable changes to this project will be documented in this file.

## Release 0.2.0

**Features**

Renamed parameters. Default `package_ensure` to `present`.

**Bugfixes**

**Known Issues**

## Release 0.1.0

**Features**

**Bugfixes**

**Known Issues**

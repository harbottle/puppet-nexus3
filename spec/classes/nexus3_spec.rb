require 'spec_helper'

describe 'nexus3' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      default_params = {}
      let(:facts) { os_facts }

      it { is_expected.to compile }

      context 'Defaults' do
        it { is_expected.to contain_class('nexus3') }
        it { is_expected.to contain_class('nexus3::install') }
        it { is_expected.to contain_class('nexus3::config') }
        it { is_expected.to contain_class('nexus3::service') }
        it { is_expected.to contain_package('nexus3') }
        it { is_expected.to contain_service('nexus3') }
      end

      context 'Do not install or manage service' do
        let(:params) do
          default_params.merge(
            service_manage: false,
          )
        end

        it { is_expected.not_to contain_service('nexus3') }
      end
    end
  end
end
